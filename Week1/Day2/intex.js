console.log("hommik!")

var sisend = 7
var tulemus
//kui on vähem kui 7, korruta kahega
//kui on suurem kui 7, siis jaga kahega
//kui ongi täpselt 7, jäta samaks
if (sisend == 7) {
    tulemus = sisend
} else if (sisend < 7) {
    tulemus = sisend * 2
} else {
    tulemus = sisend / 2
} 
console.log("Tulemus on: " + tulemus)

/*
Kui sõned on võrdsed siis prindi üks 
kui erinevad siis liida kokku ja prindi
*/

var str1 = "banaan"
var str2 = "apelsin"

if (str1 == str2) {
	console.log(str1);
} else {
	console.log(str1 + " " + str2);
}

/*
Meil on linnade nimekri aga ilma sõnata "linn"
Need palun lisada
*/

var linnad = ["Tallinn", "Tartu", "Valga"]//Defineerime nimekirja
var uuedLinnad = []//Defineerime nimekirja kuhu tuleused panna
//pop hakkab tagant pihta
//kui väärtus on suurem kui null siis täida minu käsku
while (linnad.length > 0) { //kui linnasid on listis
    var linn = linnad.pop() //võta välja viimane
    var uusLinn = linn + " linn" //ja lisa "linn" otse
    uuedLinnad.push(uusLinn) //tulemus salvest uude listi
}
console.log(uuedLinnad)

/*
    Eralda poiste ja tüdrukute nimed
*/
//ctrl + klik saad mitu kursorit, ctrl + enter tekidad uue rea.

var nimed = ["Margarita", "Mara", "Martin", "Kalev"]
var poisteNimed = []
var tydrukuteNimed = []

while (nimed.length > 0) {
	var nimi = nimed.pop()
	if (nimi.endsWith("a")) {//viimase tähe valimine
        tydrukuteNimed.push(nimi)
	}else{
        poisteNimed.push(nimi)
	}
}
console.log(poisteNimed, tydrukuteNimed)

/* Funktrioonid */

/*
Kirjuta algorütm, mis suudab ükskõik mis nime (naise, mehe) eristada
*/

var eristaja = function(nimi) {
    if (nimi.endsWith("a")) {//viimase tähe valimine
        return "tüdruk"
	}else{
        return "poiss"
	}
}
var praeguneNimi= "Peeter"
var kumb = eristaja(praeguneNimi)
console.log(kumb)

/*
    loo funktsioon mis tagastab vastuse küsimusele, 
    et tegu on numbriga !isNaN(4) //is not number. Hüüumärk pöörab true/false vastupidi
*/

var kasOnNumber = function(number) {
     if (!isNaN(number)) {
     	return true
     }else{
     	return false
     }
}

console.log( kasOnNumber(4) )
console.log( kasOnNumber("Mingi sõne") )
console.log( kasOnNumber(26544) )
console.log( kasOnNumber(6.425) )
console.log( kasOnNumber(null) )
console.log( kasOnNumber([1, 2, 3, 4]) )

/*
   Kirjuta funkt mis võtab vastu kas numbrit ja tagastab nende summa
*/

var liida = function (a, b) {
    return a + b 
}

console.log(liida(4, 5) )
console.log(liida(4, 87) )

/*
   
*/

var inimesed = {
	"Kaarel": 34,
	"Margarita": 10,
	"Suksu": [5, 4, 5],
	"Krister": {
	     vanus: 30,
	     sugu: true
    }
}
console.log(inimesed["Kaarel"])
console.log(inimesed.Kaarel)
console.log(inimesed.Krister.sugu)
console.log(inimesed.Suksu[1])