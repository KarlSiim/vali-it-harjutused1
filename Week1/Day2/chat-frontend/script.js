console.log("töötan")

// 1. ül. Alla laadida API'st tekst

var refreshMessages = async function() {
	// tean et funktsioon läks käima
     console.log("refreshMessages läks käima")
     // API aadress on string, salvestan lihtsalt muudatuse
     var APIurl = "http://138.197.191.73:8080/chat/general"
     // fetch teeb päringu serverisse (meie defineeritud aadressi)
     var request = await fetch(APIurl)
     // json() käsk vormindab meile data mugavaks json'iks
     var json = await request.json()
     
//document.querySelector('#jutt').innerHTML = JSON.stringify(json)
//kuva serverist saadud info HTML-is (ehk lehel)
    document.querySelector('#jutt').innerHTML = ""
    var sonumid = json.messages
    while (sonumid.length > 0) { // kuni sõnumeid on
    	var sonum = sonumid.shift()
        console.log(sonum)
          // lisa HTML #jutt sisse sonum.message
        document.querySelector('#jutt').innerHTML += "<p>" + sonum.user + ": " + sonum.message + "</p>"
    }
    window.scrollTo(0,document.body.scrollHeight);
}
//uuenda sõnumeid iga sekud
setInterval(refreshMessages, 1000)//1000 on 1 sekund


document.querySelector('form').onsubmit = function(event) {
	event.preventDefault() 
	//korjame formist info
	var username = document.querySelector('#username').value
	var message = document.querySelector('#message').value
	

// POST päring postitab uue andmetüki serverisse
	var APIurl = "http://138.197.191.73:8080/chat/general/new-message"
	fetch(APIurl, {
		method: "POST",
		body: JSON.stringify({user: username, message: message}),//server küsib neid tükke selles formaadis
        headers: {
        	'Accept': 'application/json',
        	'Content-Type': 'application/json'
        }
	})
}