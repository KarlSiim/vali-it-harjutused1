import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        //teema array eht massiiv
        int[] massiiv = new int[6];//lihtsam ja kiirem (miinuseks- tema pikkus on piiratud!
        ArrayList list = new ArrayList();//keerulisem, rohkem võimalusi


        // Ül1: prindi välja massiiv.
        String massiivStr = Arrays.toString(massiiv);
        System.out.println(massiivStr);

        //Ül2:Muuda kolmandal positsioonil olev number viieks
        massiiv[2] = 5;
        System.out.println(Arrays.toString(massiiv));

        //Ül3: prindi välja viimane element massiivist (aga enne määra talle ka väärtus.
        massiiv[5] = 13;
        System.out.println(massiiv[5]);

        //Ül4: Prindi välja viimane element ükskõik kui pikk see ka ei oleks
        int viimane = massiiv[massiiv.length - 1];
        System.out.println("Viimane number: " + viimane);

        //Ül5: loo uus massiiv, kus kõik 8 numbrit on kohe alguses määratud
        int[] massiiv2 = new int[]{1, 2, 3, 4, 5, 6, 7, 8};
        System.out.println(Arrays.toString(massiiv2));

        // Ül6: prindi välja ükshaaval kõik väärtused massiiv2-st (tavaline tsükkel)
        int index = 0;
        while (index < massiiv2.length) {
            System.out.println(massiiv2[index]);
            index++;
        }
        // Ül7teeme sama tsükli kiiremini kasutades for tsüklit (indexiga tsükkel)
        for (int i = 0; i < massiiv2.length; i++) {
            System.out.println(massiiv2[i]);
        }

        // Ül8:Loo stringide masiiv, mis on alguses tühi siis lisad ka veel keskele mingi sõne
        String[] stringid = new String[3];//uus string tüüpi massiiv
        stringid[1] = "Kalevipoeg";
        System.out.println(Arrays.toString(stringid));

        //1. loo massiiv kus on 100 kohta. 2. Täida massiiv järjest loeteluga alustades nulliga. 3. Prindi see välja
        int[] kohad = new int[100];
        int number = 0;
        for (int i = 0; i < kohad.length; i++) {//I++ reguleerin iga tsükliga arvule ühe juurde! While ja for tsüklid on täpselt ühesugused
            kohad[i] = number;
            number ++;

        }
        System.out.println(Arrays.toString(kohad));

        // Ül: 1. kasuta "kohad" massiivi, kus on numbrite jada juba olemas.
        // 2. loe mitu paarisarvu on
        // 3. Prindi tulemus välja
        // Kasuta tuleb nii tsüklit kui ka paarisarvu
        // Kui jagad kas  arvu operaatoriga%, siis see tagastab jäägi.
        // x % 2 tähendab, et nulli puhul on paarisarv ja 1 puhul ei ole.

        int Paaris = 0;
        for (int i = 0; i < kohad.length; i++) {
            if (i % 2 == 0){//Paaris arvudel ei jää jääki (jääk on null) seega need ongi paaris arvud
                Paaris ++; //siin iga kord kui on väärtus null võtan seda arvesse?
            }
        }
        System.out.println(Paaris);

        //Loo ArrayList ja sisesta sinna kolm numbrit ja kaks stringi.

        ArrayList list2 = new ArrayList();
        list2.add(6);
        list2.add(9);
        list2.add(3);
        list2.add("kuus");
        list2.add("kolm");
        System.out.println(list2);

        //küsi viimasest listist välja kolmas element.

        System.out.println(list2.get(2));

        //prindi kogu list välja

        System.out.println(list2);

        //prindi iga element ükshaaval välja

        for (int i = 0; i < list2.size(); i++) {
            System.out.println(list2.get(i));
        }
        //loo uus Array list, kus on 543 numbrit.
        // 1. Numbrid peavad olema suvalised, vahemikus 0-10.
        // 2. korruta iga number 5-ga.
        // 3.Salvesta see uus number samale positsioonile
        // static List arrayToList(final Object[] array) {
        //  final List l = new ArrayList(array.length);
        ArrayList suvalist = new ArrayList();
        for (int i = 0; i < 543; i++){
            //int Nr = (int)(Math.random() * 11); tulemus on sama all oleva double-ga!
            double Nr = Math.floor(Math.random() * 11);//double funktsioon jätab komakoha alles!
            suvalist.add(Nr);

        }
        System.out.println(suvalist);

        for (int i = 0; i < suvalist.size(); i++) {
            double nr = (double) suvalist.get(i);//lokaalne muutuja nr ei tule eelmisest plokist kaasa!
            double muudetudNr = nr * 5;//uue muutuja loomine uuele reale, on lihtsam koodist aru saada.
            suvalist.set(i, muudetudNr);
        }
        System.out.println("Muudetud suvalist: " + suvalist);


    }
}
