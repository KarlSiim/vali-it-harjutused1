public class Main<string> {

    public static void main(String[] args) {

        contactManager myContactManager = new contactManager();

        Contact friendIllar = new Contact();
        friendIllar.name = "Illar";
        friendIllar.phoneNumber = "53442671";
        myContactManager.addContact(friendIllar);

        Contact friendJuuli = new Contact();
        friendJuuli.name = "Juuli";
        friendJuuli.phoneNumber = "52876532";
        myContactManager.addContact(friendJuuli);

        Contact friendKarl = new Contact();
        friendKarl.name = "Karl";
        friendKarl.phoneNumber = "5299281";
        myContactManager.addContact(friendKarl);

        Contact friendPriit = new Contact();
        friendPriit.name = "Priit";
        friendPriit.phoneNumber = "52876532";
        myContactManager.addContact(friendPriit);

        Contact result = myContactManager.searchContact("Priit");
        System.out.println(result.phoneNumber);

    }

    

}
