public class Main {

    public static void main(String[] args) {
        System.out.println("Hello World!");//käitub nagu console.log == sout on (System.out.println) kiirkäsk!
        System.out.println("sout");// iga rea lõpus peab olema semikoolon

        int number = 5; //muutuja
        double n2 = 5.1; // koma kohaga muutuja tuleb kasutada double
        byte bait = 5;//8bit==1byte

        String nimi = "Karl";//String on objekt tüüpi muutuja ehk sõne
        char algusTaht = 'K';//siin kasutame ülakomasid!

        if (number == 5) {//if süntaks on täpselt sama mis JS'is
        } else {
        }
        int liitmine = (int) (number + n2);
        System.out.println(liitmine);
        System.out.println(Math.round(10.7));

        int parisNumber = Integer.parseInt( "4");
        double parisNumber2 = Double.parseDouble("4.1");

        String puuvili1 = "Banaan";
        String puuvili2 = "Alpelsin";
        if (puuvili1 == puuvili2) {
            System.out.println("Puuviljad on võrdsed!!!");
        }else{
            System.out.println("Ei ole võrdsed!");
        }

        Koer.auh();
        Koer.lausu();
        Kass.nurr();
    }

    public static int summa(int a, int b) {
        return a + b;
    }


}
