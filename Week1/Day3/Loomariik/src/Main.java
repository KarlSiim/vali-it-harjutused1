public class Main {

    public static void main(String[] args) {//lühend "psvm" tuleb see rida automaatselt
       Koer pontu = new Koer(); //"new" tekitab uue objekti
       pontu.lausu();
       pontu.maga();
       pontu.arata();

       String saba = pontu.getSaba();
        System.out.println(saba);

       Kass nurr = new Kass();
       nurr.lausu();
       nurr.maga();
       nurr.arata();

       String saba2 = nurr.getSaba();
        System.out.println(saba);
       }
    }

