import Suusataja.Suusataja;

import java.util.ArrayList;

public class Võistlus {

        ArrayList <Suusataja> voistlejad;
        int kogudistants;

        public Võistlus() {
            System.out.println("Start");
            voistlejad = new ArrayList();
            kogudistants = 20;
            for (int i = 0; i < 10; i++) {
                voistlejad.add(new Suusataja(i));

            }
            aeg();
        }
        public void aeg(){
            for (Suusataja s: voistlejad) {
                s.suusata();
                boolean lopetanud = s.kasOnLõpetanud(kogudistants);
                if (lopetanud){
                    System.out.println("Võitja on: " + s);
                    return;
                }
            }
            System.out.println(voistlejad);

            try {
                Thread.sleep(1); //arvuti termin - Thread mõttejada arvutil
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            aeg();
        }

}
