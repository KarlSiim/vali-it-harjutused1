package Suusataja;

public class Suusataja {

    int stardiNr;
    double kiirus;
    double labitudDistants;
    double dopinguKordaja;//kodune ülesanne

    public Suusataja(int i) {
        stardiNr = i;
        kiirus = Math.random()*20; //20 km/h kiirus = getRandomNumberInRange(10, 20);kiirus 10 kuni 20ni keegi ei seisa!
        labitudDistants = 0;
        dopinguKordaja = Math.random();

    }

    public void suusata() { //meetod!
        labitudDistants += kiirus / 3600;
    }

    public String toString() {
        int dist = (int) (labitudDistants);
        String nool = stardiNr + " =";
        for (int i = 0; i < dist; i++) {
            nool += "=";
        }
        nool += ">";
        return nool;
    }

    public boolean kasOnLõpetanud(int koguDistants) {
        return labitudDistants >= koguDistants;
    }
}
