import Laupaev.Laupaev;
import Pyhapaev.Pyhapaev;
import nadal.Reede;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello, World!");

       if (false && true || true){//boolean true ja false (&& on alati grupeeritud ja pannes neile esimesena sulud ümber siis saad vastusest aru)
           System.out.println("Tõene");
       }else{
           System.out.println("Väär");
       }

       Reede.koju();
       //reede on klass, koju on meetod.
        // klassi ja meetodi loomise shortcut:
        // klikka peale ja Alt+Enter

        Laupaev.peole();

        Pyhapaev.hommik();

        Pyhapaev paev = new Pyhapaev();
        paev.maga();
        paev.hommik();
        //paev.uni();kui on private siis ei saa Mainis ei saa välja kutsuda
    }
}
