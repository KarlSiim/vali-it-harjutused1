import java.util.HashMap;

public class Main {

    public static void main(String[] args) {
        System.out.println("hi!");

        //Ül: 1. Loo kolm muutujat numbritega
        // 2. Moodusta lause nende muutujatega
        // 3. Prindi see lause välja
        int aasta = 88;
        int kuu = 9;
        int paev = 16;
        String lause = "Ma sündisin " + aasta + " aastal, " + kuu + " kuul ja " + paev + " päeval.";
        System.out.println(lause);

        //%d on täisarv
        //%f on komakohaga
        //%s on string
        String parem = String.format("Ma sündisin %d aastal, %d kuul ja %d kuupäeval.", aasta, kuu, paev);
        System.out.println(parem);

        //Loe kokku mitu lampi on klassis ja pane see Hashmapi!
        //Loe kokku mitu akent on klassis ja pane see Hashmapi!
        //Loe kokku mitu inimest on klassis ja pane see Hashmapi!

        HashMap klassiAsjad = new HashMap();//järjekorda ei saa usaldada, seda ei ole olemas siin
        klassiAsjad.put("aknad", 5);
        klassiAsjad.put("lampi", 11);
        klassiAsjad.put("inimest", 22);
        System.out.println(klassiAsjad);

        //mõtle välja kolm praktilist kasutust HashMapile, kus sellist struktuuri tuleks kasutada.
        // 1. Haiglas (patsiendiID: nimi)
        // 2. Trammis ( reheliseKaardiID: kontojääk)
        // 3. Laoseis (uksi: arv)

        //prindi välja kui polju on inimesi klassis kasut. juba välja mõeldud HashMapi

        int inimest = (int) klassiAsjad.get("inimest");//kutsun ta vaälja
        System.out.println(inimest);

        // Lisa samasse HashMapi juurde mitu tasapinda on klassis, aga number enne ja siis String.
        //näiteks

        klassiAsjad.put(10, "tasapinnad");
        System.out.println(klassiAsjad);

        // loo uus HashMap kuhu saab sisestda AINULT sring: double paare ja sisesta sinna ka midagi.
        HashMap<String, Double> tyybitudHashMap = new HashMap<>();
        tyybitudHashMap.put("Number", 2.0);
        System.out.println(tyybitudHashMap.get("Number"));

        // Ül: swith
        int rongiNr = 50;
        String suund = null;
        switch (rongiNr) {
            case 50:
                suund = "Pärnu";
            case 55:
                suund = "Haapsalu";
                break;
            case 10:
                suund = "Vormsi";
                break;
        }
        System.out.println(suund);

        // FOREACH
        int[] mingidNumbrid = new int[]{8, 4, 345, 7645, 2345677};
        for (int i = 0; i < mingidNumbrid.length; i++) {
            System.out.println(mingidNumbrid[i]);
        }
        System.out.println("------------------");

        for ( int nr : mingidNumbrid){
            System.out.println(nr);
        }

        // Õpilane saab töös punkte 0-100ni
        // kui punkte on alla 50 kukub õpilane tööst läbi
        //vastasel juhul on hinne täisarvuline punktid/20
        // 100 punkti => 5
        // 80 punkti => 4
        // 67 punkti => 3
        // 50 punkti =>2

        int punkte = 99;
        if (punkte > 100 || punkte < 0){
            throw new Error();
        }
        switch ((int) Math.round(punkte / 20.0)){
            case 5:
                System.out.println("suurepärane");
                break;
            case 4:
                System.out.println("hea");
                break;
            case 3:
                System.out.println("rahuldav");
                break;
            case 2:
                System.out.println("ee....kas õppisid ka?");
                break;
            default:
                System.out.println("Kukkusid läbi!");
        }
    }
}