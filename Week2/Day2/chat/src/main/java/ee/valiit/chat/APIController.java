package ee.valiit.chat;

import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

//see chat on nüüd konroller, Cross lubab kõit tüüpi päringud!
@RestController
@CrossOrigin
public class APIController {
    HashMap <String, ChatRoom> rooms = new HashMap();

    public APIController(){//konstruktor on meetod mis käivitub automaatselt
        rooms.put("general", new ChatRoom("general"));
        rooms.put("random", new ChatRoom("random"));
        rooms.put("materjalid", new ChatRoom("materjalid"));

    }

    @GetMapping("/chat/{room}")//get tüüpi päring
    ChatRoom chat1 (@PathVariable String room){//loome Chatroomi
        return rooms.get(room);
    }
    @PostMapping("chat/room/new-message")
        void newMessage(@RequestBody ChatMessage msg, @PathVariable String room){//vastuvõtmine sulgudes
          rooms.get(room).addMessage(msg);
    }//loo vel üks uus logilist
    // lihtsam ülesanne: kui sõnumi saarad serverisse

}
