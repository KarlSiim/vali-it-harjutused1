package ee.valiit.chat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ChatApplicationTests {

	@Autowired//käsk, ühendab need alumised automaatselt siia sisse
			JdbcTemplate jdbcTemplate;

	@Test
	public void contextLoads() throws Exception {
		ChatApplication ca = new ChatApplication();//käitvitame run meetodi
		ca.run();
		List<Map<String, Object>> tables = jdbcTemplate.queryForList("SELECT * FROM pg catalog.pg tables;");
		System.out.println(tables);
	}

}
