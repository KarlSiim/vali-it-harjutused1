package ee.valiit.chat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;

//see chat on nüüd konroller, Cross lubab kõit tüüpi päringud!
@RestController
@CrossOrigin
public class APIController {


    @Autowired//käsk, ühendab need alumised automaatselt siia sisse
            JdbcTemplate jdbcTemplate;


    @GetMapping("/chat/{room}")
//get tüüpi päring. SQL käsud siia!
    ArrayList<ChatMessage> chat(@PathVariable String room) {
        try {
            String sqlKask = "SELECT * FROM messages WHERE room='" + room + "'";
            ArrayList<ChatMessage> messages = (ArrayList) jdbcTemplate.query(sqlKask, (resultSet, rownum) -> {
                String username = resultSet.getString("username");
                String message = resultSet.getString("message");
                String pilt = resultSet.getString("pilt");
                return new ChatMessage(username, message, pilt);
            });
            return messages;
        } catch (DataAccessException err) {//vea otsing ja annab all tabelis kohe vea teate kui tabel pole valmis
            System.out.println("Table was not ready");
            return new ArrayList();
        }
    }

    @PostMapping("/chat/{room}/new-message")
    void newMessage(@RequestBody ChatMessage msg, @PathVariable String room) {//vastuvõtmine sulgudes
       String sqlKast="INSERT INTO messages (username, message, room, pilt) VALUES ('" + msg.getUsername() + "','" + msg.getMessage() + "','" + room + "','" + msg.getPilt() + "')";
       jdbcTemplate.execute(sqlKast);
    }

}
