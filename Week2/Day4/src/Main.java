import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello World!");

        ArrayList<Athlete> sportlased = new ArrayList<>();
        sportlased.add(new Skydiver("Tamm"));
        sportlased.add(new Skydiver("Lehtpuu"));
        sportlased.add(new Skydiver("Koni"));
        sportlased.add(new Runner("Porgand"));
        sportlased.add(new Runner("Hani"));
        sportlased.add(new Runner("Mooses"));

        //varjur1.getClass().getDeclaredFields();

        for (Athlete sportlane: sportlased) {
            System.out.println(sportlane.eesnimi);
            System.out.println(sportlane.perenimi);
            System.out.println(sportlane.vanus);
            System.out.println(sportlane.sugu);
            System.out.println(sportlane.pikkus);
            System.out.println(sportlane.kaal);
        }
    }
}
