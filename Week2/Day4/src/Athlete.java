public abstract class Athlete {

    String eesnimi;
    String perenimi;
    int vanus;
    String sugu;
    double pikkus;
    double kaal;

    public abstract void perform();

    public Athlete(String perenimi){
        this.perenimi=perenimi;
    }

}
