var url = "http://localhost:8080/list"//lõppu paneme , et oleks iga url unikaalne, mõtleme ise välja. Lihtsalt serveris üks ruut
//ei saa kasutada koguaeg üjte ja sama aadressi muidu lähevad omavahel sassi.
document.querySelector("#form").onsubmit = function(e){
   console.log("onsubmit toimib")
   e.preventDefault()//ei refreshi autpomaatselt. Nüüd tahame saatma hakata

   var nimi = document.querySelector("#nimi").value
   var vanus = document.querySelector("#vanus").value
   fetch(url, {
   method: "POST",//meetod on post veel tahab saada body!
   body: JSON.stringify({nimi, vanus}),//konverteeriib objekti loetavaks stringiks
   headers:{
      'Accept': 'application/json',
      'Content-type':'application/json'
   }
   })

}