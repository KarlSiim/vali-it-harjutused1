package ee.valiit.kasutjatelist;

public class Kasutaja {
    private String nimi;
    private int vanus;

    public Kasutaja(){}

    public String getNimi() {
        return nimi;
    }

    public int getVanus() {
        return vanus;
    }
}
