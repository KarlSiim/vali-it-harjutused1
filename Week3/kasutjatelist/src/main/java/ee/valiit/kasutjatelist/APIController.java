package ee.valiit.kasutjatelist;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@CrossOrigin//mis tüüpi teksti vastu võetakse? lubab kõik vastu võtta aga ei ole turvaline.
@RestController//weebi kontroller saaks aru et see sõnum on üldse tema jaoks mõeldud
public class APIController {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @PostMapping ("/list") //see võtab konkreetselt vastu list päringu!
    public void handleNewListItem(@RequestBody Kasutaja kasutaja) {//et java oskaks konverteerida?
        System.out.println("handleNewListItem");
        System.out.println("Kasutja nimi: " + kasutaja.getNimi());
        String sqlKask = "INSERT INTO kasutajad (nimi, vanus) VALUES ('"
                + kasutaja.getNimi() + "', " + kasutaja.getVanus() + ");";
                //see rohekisega on SQL-i käsk see käsk on nüüd vaja POSTSQL-i saata
        jdbcTemplate.execute(sqlKask);
        System.out.println("Sisestamine õnnestus");
    }
}
